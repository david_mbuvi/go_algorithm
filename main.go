
package main

import "fmt"

//const
const Alphabet =26
//Node trie
type Node struct {
  children [Alphabet]*Node
  isEnd bool
}

//trie
type Trie struct {
	root *Node
}

//function init
func InitTrie() *Trie  {
	result := &Trie{root: &Node{}}
	return result
}

func (t *Trie) Insert(w string)  {
	wordlength := len(w)
	currentNode := t.root
	for i:=0; i<wordlength;i++{
		charIndex := w[i]-'a'
		if currentNode.children[charIndex] == nil{
			currentNode.children[charIndex] =&Node{}
		}
		currentNode = currentNode.children[charIndex]
	}
	currentNode.isEnd = true
}

func (t *Trie) Search(w string) bool {
	wordlength := len(w)
	currentNode := t.root
	for i:=0; i<wordlength;i++{
		charIndex := w[i]-'a'
		if currentNode.children[charIndex] == nil{
			return false
		}
		currentNode = currentNode.children[charIndex]
	}
	if currentNode.isEnd == true{
		return true
	}
	return false
}

func main()  {

	myInit := InitTrie()

	toAdd := []string{
		"aro",
		"arc",
		"cat",
		"hot",
	}

	for _,v:= range toAdd{
		myInit.Insert(v)
	}

	fmt.Println(myInit.Search("hot"))
}
